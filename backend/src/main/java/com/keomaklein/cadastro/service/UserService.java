package com.keomaklein.cadastro.service;

import java.util.List;

import com.keomaklein.cadastro.domain.orm.User;

public interface UserService {
	User getById(Integer id);
	List<User >getAll();
	User save(User user);
	void delete(Integer id);
}
