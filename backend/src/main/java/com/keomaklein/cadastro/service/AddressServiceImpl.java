package com.keomaklein.cadastro.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.keomaklein.cadastro.api.resource.AddressConverter;
import com.keomaklein.cadastro.client.AddressClient;
import com.keomaklein.cadastro.domain.orm.Address;
import com.keomaklein.cadastro.domain.repository.AddressRepository;

@Service
public class AddressServiceImpl implements AddressService {

	@Autowired
	private AddressRepository addressRepository;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Override
	public Address getById(Integer id) {
		Optional<Address> address = addressRepository.findById(id);
		if (address.isEmpty()) {
			throw new RuntimeException("Address not found!");
		}
		return address.get();
	}

	
	@Override
	public Address save(Address address) {
		return addressRepository.save(address);
	}


	@Override
	public Address getAddressByZipCode(String zipCode) {
		try {
			AddressClient addressClient = restTemplate.getForObject("https://viacep.com.br/ws/"+zipCode+"/json/", AddressClient.class);
			return AddressConverter.parse(addressClient);
		} catch (Exception e) {
			System.err.println(e);
		}
		return null;
	}

	@Override
	public List<Address> getAll() {
		Iterable<Address> adresses = addressRepository.findAll();	
		List<Address> addressList = new ArrayList<>();
		adresses.forEach(addressList::add);
		return addressList;
	}

	@Override
	public List<Address> getByUserId(Integer id) {		
		return addressRepository.getByUserId(id);
	}

	@Override
	public void delete(Integer id) {
		Optional<Address> address = addressRepository.findById(id);
		if (address.isEmpty()) {
			throw new RuntimeException("User not found!");
		}
		addressRepository.delete(address.get());
	}
}
