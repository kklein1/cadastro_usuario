package com.keomaklein.cadastro.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.keomaklein.cadastro.domain.orm.User;
import com.keomaklein.cadastro.domain.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public User getById(Integer id) {
		Optional<User> user = userRepository.findById(id);
		if (user.isEmpty()) {
			throw new RuntimeException("User not found!");
		}
		return user.get();
	}

	@Override
	public List<User> getAll() {
		Iterable<User> users = userRepository.findAll();	
		List<User> userList = new ArrayList<>();
		users.forEach(userList::add);
		return userList;
	}
	
	@Override
	public User save(User user) {
		return userRepository.save(user);
	}
	
	@Override
	public void delete(Integer id) {
		Optional<User> user = userRepository.findById(id);
		if (user.isEmpty()) {
			throw new RuntimeException("User not found!");
		}
		userRepository.delete(user.get());
	}

}
