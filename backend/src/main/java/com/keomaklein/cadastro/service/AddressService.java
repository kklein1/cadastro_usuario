package com.keomaklein.cadastro.service;

import java.util.List;

import com.keomaklein.cadastro.domain.orm.Address;

public interface AddressService {
	Address getById(Integer id);
	List<Address>getAll();
	List<Address>getByUserId(Integer id);
	Address getAddressByZipCode(String zipCode);
	Address save(Address address);
	void delete(Integer id);

}
