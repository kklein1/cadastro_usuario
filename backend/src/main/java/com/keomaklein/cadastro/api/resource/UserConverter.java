package com.keomaklein.cadastro.api.resource;


import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.keomaklein.cadastro.domain.orm.User;


@Component
public class UserConverter {
	public static UserDto parse(User user) {
		UserDto userDto = new UserDto();
		userDto.setCpf(user.getCpf());
		userDto.setName(user.getName());
		userDto.setPhone(user.getPhone());
		userDto.setId(user.getId());
		return userDto;
	}  

	public static User parse(UserDto userDto) {
		User user= new User();
		user.setCpf(userDto.getCpf());
		user.setName(userDto.getName());
		user.setPhone(userDto.getPhone());
		user.setId(userDto.getId());
		return user;
	}  
	
	public static List<UserDto> parseList(List<User> users) {
		List<UserDto> dtoList = new ArrayList<>();
		for (User user : users) {
			dtoList.add(parse(user));
		}
		return dtoList;
	}
	
	public static List<User> parseListDto(List<UserDto> usersDto) {
		List<User> userList = new ArrayList<>();
		for (UserDto userDto : usersDto) {
			userList.add(parse(userDto));
		}
		return userList;
	}
	
}
