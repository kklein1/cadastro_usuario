package com.keomaklein.cadastro.api.resource;


import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.keomaklein.cadastro.client.AddressClient;
import com.keomaklein.cadastro.domain.orm.Address;


@Component
public class AddressConverter {
	public static AddressDto parse(Address address) {
		AddressDto addressDto = new AddressDto();
		addressDto.setId(address.getId());
		addressDto.setName(address.getName());
		addressDto.setNeighborhood(address.getNeighborhood());
		addressDto.setNumber(address.getNumber());
		addressDto.setState(address.getState());		
		addressDto.setStreet(address.getStreet());
		addressDto.setUser(address.getUser());
		addressDto.setZipCode(address.getZipCode());
		return addressDto;
	}  
	
	public static Address parse(AddressClient addressClient) {
		Address address = new Address();
		address.setNeighborhood(addressClient.getBairro());
		address.setStreet(addressClient.getLogradouro());
		address.setState(addressClient.getUf());
		address.setZipCode(addressClient.getCep());
		return address;
	}  
	
	public static List<AddressDto> parseList(List<Address> adresses) {
		List<AddressDto> dtoList = new ArrayList<>();
		for (Address address : adresses) {
			dtoList.add(parse(address));
		}
		return dtoList;
	}
	
}
