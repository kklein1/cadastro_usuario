package com.keomaklein.cadastro.api.resource;

import com.keomaklein.cadastro.domain.orm.User;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AddressDto {
	private Integer id;	
	private String name;
	private String street;
	private String number;
	private String zipCode;
	private String neighborhood;
	private String state;
	private User user;
}
