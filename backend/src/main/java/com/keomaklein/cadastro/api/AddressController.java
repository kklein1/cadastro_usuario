package com.keomaklein.cadastro.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.keomaklein.cadastro.api.resource.AddressConverter;
import com.keomaklein.cadastro.domain.orm.Address;
import com.keomaklein.cadastro.service.AddressService;

import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
@RequestMapping("address")
public class AddressController {

	@Autowired
	AddressService addressService;
	
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?>  getById(@PathVariable Integer id) {
		return ResponseEntity.status(HttpStatus.OK).body(AddressConverter.parse(addressService.getById(id)));
    }


	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getAll(@PathVariable Integer id) {
		return ResponseEntity.status(HttpStatus.OK).body(AddressConverter.parseList(addressService.getByUserId(id)));
	}
	
	
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/zipCode/{zipCode}", method = RequestMethod.GET)
    public ResponseEntity<?> getAddressByZipCode(@PathVariable String zipCode) {
		return ResponseEntity.status(HttpStatus.OK).body(AddressConverter.parse(addressService.getAddressByZipCode(zipCode)));
    }

    
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?>  save(@RequestBody Address address) {
    	try {
    		return ResponseEntity.status(HttpStatus.CREATED).body(addressService.save(address));			
		} catch (DataIntegrityViolationException e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e);
		}
    }
	
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> delete(@PathVariable Integer id) {
		addressService.delete(id);
		return ResponseEntity.ok().build();
	}
}