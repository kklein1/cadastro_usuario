package com.keomaklein.cadastro.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.keomaklein.cadastro.domain.orm.Address;

public interface AddressRepository extends CrudRepository<Address, Integer> {

	@Query(value = "SELECT ad FROM Address ad WHERE ad.user.id=:id")
	List<Address> getByUserId(Integer id);	
	
}
