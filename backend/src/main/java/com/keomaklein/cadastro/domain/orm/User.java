package com.keomaklein.cadastro.domain.orm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "USER", uniqueConstraints={@UniqueConstraint(columnNames="DS_CPF")})
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "ID_USER", nullable = false)
	private Integer id;
	
	@NotBlank
	@Column(name = "DS_NAME")
	private String name;
	
	@NotBlank
	@Column(name = "DS_PHONE")
	private String phone;
		
	@NotBlank
	@Column(name = "DS_CPF")
	private String cpf;
}
