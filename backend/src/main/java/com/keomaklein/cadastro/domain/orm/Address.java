package com.keomaklein.cadastro.domain.orm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "ADDRESS", uniqueConstraints={@UniqueConstraint(columnNames= {"DS_ZIP_CODE", "ID_USER"})})
public class Address implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name = "ID_ADDRESS", nullable = false)
	private Integer id;
	
	@NotBlank
	@Column(name = "DS_NAME")
	private String name;
	
	@NotBlank
	@Column(name = "DS_STREET")
	private String street;
	
	@NotBlank
	@Column(name = "DS_NUMBER")
	private String number;
	
	@NotBlank
	@Column(name = "DS_ZIP_CODE")
	private String zipCode;
	
	@NotBlank
	@Column(name = "DS_NEIGHBORHOOD")
	private String neighborhood;
	
	@Column(name = "DS_STATE")
	private String state;
	
	@ManyToOne
	@JoinColumn(name = "ID_USER")
	private User user;
}
