package com.keomaklein.cadastro.domain.repository;

import org.springframework.data.repository.CrudRepository;

import com.keomaklein.cadastro.domain.orm.User;

public interface UserRepository extends CrudRepository<User, Integer> {

}
