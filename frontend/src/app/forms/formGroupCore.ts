import { FormArray, FormBuilder, FormGroup } from "@angular/forms";
import { FormGroupInterface } from "./formGroupInterface.interface";

export abstract class FormGroupCore<Model> implements FormGroupInterface<Model> {
    formBuilder = new FormBuilder();
  
    abstract toForm(model: Model, options?: any);
  
    fromForm(model: Model, form: FormGroup) {
      return Object.assign(model, form.value);
    }
    
    updateFormArray(formArray: FormArray, itens: any[], fc: any) {
      this.clearFormArray(formArray);
      if (itens) {
        itens.forEach(item => formArray.push(fc(item)));      
      }
    }
    
    clearFormArray(formArray: FormArray) {
      while (formArray.length != 0) {
        formArray.removeAt(0);
      }
    }
  
    newFormArray(itens: any[], validators = [], funcaoGeradora: (item: Model) => FormGroup) {
      if (!itens || (itens && !itens.length)) {
        return this.formBuilder.array([], validators);
      }
      return new FormArray(itens.map(item => funcaoGeradora(item), validators));
    }
  
  }
  