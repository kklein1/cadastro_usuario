import { FormGroup, Validators } from "@angular/forms";
import { Address } from "../models/address.model";
import { User } from "../models/user.model";
import { FormGroupCore } from "./formGroupCore";

export class AddressForm extends FormGroupCore<Address> {

    constructor() {
        super();
    }

    toForm(model: Address) {
        return this.formBuilder.group({
            id: [model.id, []],
            name: [model.name, [Validators.required]],
            street: [model.street, [Validators.required]],
            number: [model.number, [Validators.required]],
            neighborhood: [model.neighborhood, [Validators.required]],            
            state: [model.state, [Validators.required]],            
            zipCode: [model.zipCode, [Validators.required]],            
            user: [model.user, []],
        });
    }

    updateForm(model: Address, user: User, form: FormGroup) {
        form.patchValue({
            id: model ? model.id : null,
            name: model ? model.name : null,
            street: model ? model.street : null,
            number: model ? model.number : null,
            neighborhood: model ? model.neighborhood : null,
            state: model ? model.state : null,
            zipCode: model ? model.zipCode : null,   
            user: user        
        });
    }

    updateFormSearchZip(model: Address, form: FormGroup) {
        form.patchValue({            
            street: model ? model.street : null,            
            neighborhood: model ? model.neighborhood : null,
            state: model ? model.state : null,
            zipCode: model ? model.zipCode : null            
        });
    }

    fromForm(model: Address, form: FormGroup) {
        let mod = Object.assign(model, form.value);
        return mod;
    }
}