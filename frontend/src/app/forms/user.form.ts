import { FormGroup, Validators } from "@angular/forms";
import { User } from "../models/user.model";
import { FormGroupCore } from "./formGroupCore";

export class UserForm extends FormGroupCore<User> {

    constructor() {
        super();
    }

    toForm(model: User) {
        return this.formBuilder.group({
            id: [model.id, []],
            name: [model.name, [Validators.required]],
            phone: [model.phone, [Validators.required]],
            cpf: [model.cpf, [Validators.required]],
            adresses: [model.adresses, []],            
        });
    }

    updateForm(model: User, form: FormGroup) {
        form.patchValue({
            id: model.id,
            name: model.name,
            phone: model.phone,
            cpf: model.cpf,
            adresses: model.adresses,            
        });
    }

    fromForm(model: User, form: FormGroup) {
        let mod = Object.assign(model, form.value);
        return mod;
    }
}