import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class AppService {
    refreshTable: EventEmitter<boolean> = new EventEmitter<boolean>();
    refreshAdresses: EventEmitter<boolean> = new EventEmitter<boolean>();

    constructor() {
    }

    setRefreshTable(): void {
        this.refreshTable.emit(true);
    }

    setRefreshAdresses(): void {
        this.refreshAdresses.emit(true);
    }
}