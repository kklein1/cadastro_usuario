import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";

@Injectable({
    providedIn: 'root'
})
export class AddressService {
    resource: String = "address";

    constructor(private http: HttpClient) { }

    getAddress(idAddress: number) {
        return this.http.get<any>(`${environment.webBackend}/${this.resource}/${idAddress}`);
    }

    getAddressByZipCode(zipCode: string) {
        return this.http.get<any>(`${environment.webBackend}/${this.resource}/zipCode/${zipCode.replace(/\D/g,'')}`);
      }

    getAdressesByUser(idUser: number) {
        return this.http.get<any>(`${environment.webBackend}/${this.resource}/user/${idUser}`);
    }

    getAdresses() {
        return this.http.get<any>(`${environment.webBackend}/${this.resource}`);
    }

    save(form: any) {
        return this.http.post<any>(`${environment.webBackend}/${this.resource}`, form);
    }
    
    update(form: any) {
        return this.http.put<any>(`${environment.webBackend}/${this.resource}/${form.id}`, form);
    }

    delete(idAddress: number) {
        console.log("delete: ", idAddress);
        return this.http.delete(`${environment.webBackend}/${this.resource}/${idAddress}`, {});
    }
}