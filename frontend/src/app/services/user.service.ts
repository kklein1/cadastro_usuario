import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";

@Injectable({
    providedIn: 'root'
})
export class UserService {
    resource: String = "user";

    constructor(private http: HttpClient) { }

    getUser(idUser: number) {
        return this.http.get<any>(`${environment.webBackend}/${this.resource}/${idUser}`);
    }

    getUsers(): Observable<any> {
        return this.http.get<any>(`${environment.webBackend}/${this.resource}`);
    }

    save(form: any) {
        return this.http.post<any>(`${environment.webBackend}/${this.resource}`, form);
    }

    update(form: any) {
        return this.http.put<any>(`${environment.webBackend}/${this.resource}/${form.id}`, form);
    }

    delete(idUser: number) {
        console.log("delete: ", idUser);
        return this.http.delete(`${environment.webBackend}/${this.resource}/${idUser}`, {});
    }
}