import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { RotesEnum } from 'src/app/constants/rotes.enum';
import { UserForm } from 'src/app/forms/user.form';
import { User } from 'src/app/models/user.model';
import { AddressRegisterComponent } from 'src/app/pages/address-register/address-register.component';
import { AddressService } from 'src/app/services/address.service';
import { AppService } from 'src/app/services/app.service';
import { UserService } from 'src/app/services/user.service';
import { SnackBarComponent } from '../components/snack-bar/snack-bar.component';

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.scss']
})
export class UserRegisterComponent implements OnInit {

  form: any;
  idUser;
  adresses = [];
  durationInSeconds = 10;
  userDuplcate = false;
  
  constructor(
    private router: Router,  
    private route: ActivatedRoute,
    public dialog: MatDialog, 
    private userService: UserService,
    private addressService: AddressService,
    private appService: AppService,
    private ref: ChangeDetectorRef,
    private _snackBar: MatSnackBar
    ) { }

  ngOnInit(): void {
    this.form = new UserForm().toForm(new User());
    this.idUser = this.route.snapshot.params['id'];    
    if (this.idUser) {
      this.getUser(this.idUser);
    } 
    this.refreshAdresses();
  }

  getUser(id){
    this.userService.getUser(id).subscribe((result: any) => {      
      new UserForm().updateForm(result, this.form);      
      this.getAdresses(id);
    }, error => {      
      console.log("Error: ", error);
    });
  }

  cancel() {
    this.router.navigateByUrl(RotesEnum.HOME);
  }

  openDialog(address = null) {
    const dialogRef = this.dialog.open(AddressRegisterComponent,{data: {idUser: this.idUser, id: address ? address.id : null}});
    dialogRef.afterClosed().subscribe(result => {      
      this.getAdresses(this.idUser);
    });
  }

  getAdresses(idUser){
    this.addressService.getAdressesByUser(idUser).subscribe((result: any) => {      
      this.adresses = result;      
    }, error => {      
      console.log("Error: ", error);
    });
  }

  save() {
    this.inputTouched();
    if (this.form.valid) {
      this.userService.save(new UserForm().fromForm(new User(), this.form)).subscribe(
        result => {
          this.userDuplcate = false
          this.openSnackBar("Usuário " + (this.form.controls.id.value ? "alterado" : "incluído") + " com sucesso!", "");
          if (this.idUser) {
            this.router.navigateByUrl(RotesEnum.HOME);
          }else{            
            this.router.navigateByUrl(RotesEnum.USER+"/"+result.id);
          }
        }, error => {
          switch (error.error.cause.errorCode) {
            case 1062: this.userDuplcate = true; break;              
            default: this.userDuplcate = false; break;
          }
        });
    } else {
      console.log("Form inválido!", this.form);
    }
  }

  inputTouched(){
    this.form.get('name').markAsTouched();
    this.form.get('cpf').markAsTouched();
    this.form.get('phone').markAsTouched();
  }
  
  deleteAddress(id){     
    this.addressService.delete(id).subscribe((result: any) => {      
      this.getAdresses(this.idUser);
      this.openSnackBar("Endereço excluído com sucesso!", "");
    }, error => {      
      console.log("Error: ", error);
    });
  }
  
  refreshAdresses(){
    this.appService.refreshAdresses.subscribe(refresh => {      
      this.getAdresses(this.idUser);
      this.ref.detectChanges();
    })
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
      verticalPosition: 'top',
      horizontalPosition: 'end',
      panelClass: ['mat-toolbar', 'mat-primary']
    });
  }
}
