import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { RotesEnum } from 'src/app/constants/rotes.enum';
import { User } from 'src/app/models/user.model';
import { AppService } from 'src/app/services/app.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {

  users: User[];
  userAddresErr = false;

  constructor(
    private router: Router,
    private userService: UserService,
    private appService: AppService,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.getUsers();
  }

  getUsers(){
    this.userService.getUsers().subscribe((result: any) => {      
      this.users = result;
      this.appService.setRefreshTable();
    }, error => {      
      console.log("Error: ", error);
    });
  }

  create(){
    this.router.navigateByUrl(RotesEnum.USER);
  }
  
  edit(id){
    this.router.navigateByUrl(RotesEnum.USER+"/"+id);    
  }

  delete(id){     
    this.userService.delete(id).subscribe((result: any) => {      
      this.getUsers();
      this.appService.setRefreshTable();
    }, error => {      
      console.log(error.error.cause.errorCode);
      switch (error.error.cause.errorCode) {
        case 1451:
          this.openSnackBar("Usuário possui endereço cadastrado, necessário excluir antes de continuar!", ""); 
          this.userAddresErr = true; 
        break;              
        default: this.userAddresErr = false; break;
      }
    });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
      verticalPosition: 'top',
      horizontalPosition: 'end',
      panelClass: ['mat-toolbar', 'mat-primary']
    });
  }

}