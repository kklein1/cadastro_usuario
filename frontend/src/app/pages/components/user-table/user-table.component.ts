import { Component, Input, OnInit, Output, EventEmitter, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { AppService } from 'src/app/services/app.service';


@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.scss']
})
export class UserTableComponent implements OnInit {
  
  _dataSource;

  @Input() users;     
  @Output() edit = new EventEmitter();
  @Output() delete = new EventEmitter();
  @ViewChild('myTable') myTable: MatTable<any>;
  
  displayedColumns: string[] = ['id', 'name', 'phone', 'cpf', 'actions'];

  constructor(    
    private appService: AppService,
    private ref: ChangeDetectorRef,
    ) {
  }
    
  ngOnInit(): void {  
    this._dataSource = new MatTableDataSource(this.users);
    this.refreshTable();
  }
  
  get dataSource (){    
    this._dataSource = new MatTableDataSource(this.users);
    return this._dataSource;
  }
  
  refreshTable(){
    console.log("refreshTable");  
    this.appService.refreshTable.subscribe(refresh => {      
      this.ref.detectChanges();
    })
  }

}
