import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { RotesEnum } from 'src/app/constants/rotes.enum';
import { AddressForm } from 'src/app/forms/address.form';
import { Address } from 'src/app/models/address.model';
import { User } from 'src/app/models/user.model';
import { AddressService } from 'src/app/services/address.service';
import { AppService } from 'src/app/services/app.service';

@Component({
  selector: 'app-address-register',
  templateUrl: './address-register.component.html',
  styleUrls: ['./address-register.component.scss']
})
export class AddressRegisterComponent implements OnInit {

  formAddress: any;
  zipCodeNotFound = false;
  zipCodeDuplcate = false;

  constructor(
    public dialogRef: MatDialogRef<AddressRegisterComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: any,
    private addressService: AddressService,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.formAddress = new AddressForm().toForm(new Address());
    console.log("data: ", this.data);

    if (this.data.id) {
      this.getAddress(this.data.id);
    } 
  }

  get form(){
    return this.formAddress;
  }
  
  getAddress(id){
    this.addressService.getAddress(id).subscribe((result: any) => {      
      new AddressForm().updateForm(result, new User(), this.formAddress);      
    }, error => {      
      console.log("Error: ", error);
    });
  }

  findZipCode() {
    console.log(this.formAddress)
    if (this.formAddress.controls.zipCode.value && this.formAddress.controls.zipCode.value.length == 8) {
      if (this.formAddress.controls.zipCode.status == "VALID") {
        this.addressService.getAddressByZipCode(this.formAddress.controls.zipCode.value).subscribe(
          retorno => {            
            if(retorno.zipCode){
              new AddressForm().updateFormSearchZip(retorno, this.formAddress);
              this.zipCodeNotFound = false;
            }else{
              this.openSnackBar("CEP não encontrado!", "");
              this.zipCodeNotFound = true;
              this.form.get('zipCode').markAsTouched();
              this.formAddress.controls.zipCode.setErrors({ 'invalid': true });
            }
          }, error => {
            console.log('Erro: ', error);
            this.formAddress.controls.zipCode.setErrors({ 'invalid': true });
          });
      }
    }
  }

  save() {
    this.inputTouched();
    if (this.formAddress.valid) {   
      let address = new AddressForm().fromForm(new Address(), this.formAddress);
      let user = new User();
      user.id = this.data.idUser; 
      address.user = user;
      this.addressService.save(address).subscribe(
        result => {   
          this.zipCodeDuplcate = false;
          this.openSnackBar("Endereço " + (address.id ? "alterado" : "incluído") + " com sucesso!", "");
          this.dialogRef.close();
        }, error => {          
          switch (error.error.cause.errorCode) {
            case 1062: 
            this.zipCodeDuplcate = true; 
            this.openSnackBar("CEP duplicado!", "");
            break;              
            default: this.zipCodeDuplcate = false; break;
          }
        });
    } else {
      console.log("Form inválido!", this.formAddress);
    }
  }

  inputTouched(){
    this.form.get('name').markAsTouched();
    this.form.get('street').markAsTouched();
    this.form.get('number').markAsTouched();
    this.form.get('neighborhood').markAsTouched();
    this.form.get('state').markAsTouched();
    this.form.get('zipCode').markAsTouched();    
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
      verticalPosition: 'top',
      horizontalPosition: 'end',
      panelClass: ['mat-toolbar', 'mat-primary']
    });
  }
}
