import { Address } from "./address.model";

export class User {
  id: number;
  name: string;
  phone: string;
  cpf: string;
  adresses: Address[];

  constructor() {
    this.adresses = [];
  }
}
