import { User } from "./user.model";

export class Address {
    id: number;
    name: string;
    street: string;
    number: string;
    zipCode: string;
    neighborhood: string;
    state: string;
    user: User;
    
    constructor() {
        this.user = new User();
    }
}
