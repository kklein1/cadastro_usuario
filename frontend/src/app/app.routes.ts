import { Routes } from "@angular/router";
import { RotesEnum } from "./constants/rotes.enum";
import { AddressRegisterComponent } from "./pages/address-register/address-register.component";
import { HomeComponent } from "./pages/home/home.component";
import { UserRegisterComponent } from "./pages/user-register/user-register.component";

export const ROUTES: Routes = [
  { path: '', redirectTo: RotesEnum.HOME, pathMatch: 'full' },
  { path: RotesEnum.HOME, component: HomeComponent },  
  { path: RotesEnum.USER, component: UserRegisterComponent},
  { path: RotesEnum.USER +'/:id', component: UserRegisterComponent},
  { path: RotesEnum.ADDRESS, component: AddressRegisterComponent },
]