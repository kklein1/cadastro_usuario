# Projeto teste Fullstack MD

## Sistema para cadastro de usuário e endereços.

### Tecnologias utilizadas no Backend:
- Java utilizando Spring Boot
- Lombok
- Banco de dados MySql
- Docker

### Tecnologias utilizadas no Frontend:
- Angular 11.0.5
- ngx-mask 11.1.4
- Angular Material 11.0.3

O Frontend se comunica com o Backend via protocolo HTTP utilizando padrão de comunicação REST.

Para subir a aplicação devemos seguir os seguintes passos:
 ```console
git clone https://gitlab.com/kklein1/cadastro_usuario.git cadastro_usuario
cd cadastro_usuario
cd config_db_docker
docker-compose up -d
cd ..
cd frontend && npm install && ng serve
 ```

 Para subir o Backend utilize uma IDE.